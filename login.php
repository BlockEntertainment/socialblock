<? require("db.php"); ?>

<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login, registration forms">
    <meta name="author" content="Seong Lee">

    <title>SocialBlock</title>

    <!-- Stylesheets -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/animation.css" rel="stylesheet">
	<link href="css/checkbox/orange.css" rel="stylesheet">
	<link href="css/preview.css" rel="stylesheet">
	<link href="css/authenty.css" rel="stylesheet">
	<link href="css/template-login.css" rel="stylesheet">

	<!-- Font Awesome CDN -->
	<link href="css/font-awesome.css" rel="stylesheet">
	
	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css">

  <style type="text/css"></style><script type="text/javascript" src="chrome-extension://bfbmjmiodbnnpllbbbfblcplfjjepjdn/js/injected.js"></script></head>
  <body>

  		<?
  			if(!$_GET['login']){ require("inc/login/login.inc.php"); }
  			if($_GET['login'] == "login"){ require("inc/login/login.inc.php"); }
  			if($_GET['login'] == "logout"){ require("inc/login/logout.inc.php"); }
  			if($_GET['login'] == "register"){ require("inc/login/register.inc.php"); }
  			if($_GET['login'] == "password"){ require("inc/login/password.inc.php"); }
  		?>

    <!-- js library -->
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    	<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.icheck.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		
		<!-- authenty js -->
		<script src="js/authenty.js"></script>
		
		<!-- preview scripts -->
		<script src="js/preview/jquery.malihu.PageScroll2id.js"></script>
		<script src="js/preview/jquery.address-1.6.min.js"></script>
		<script src="js/preview/init.js"></script>

</body>
</html>