<? require("db.php"); ?>
<? if(!isset($_COOKIE['user_id'])){header("Location:login.php?login=login&danger=1");} ?>
<?
    $user = mysqli_query($db, "SELECT * FROM users WHERE id = '".$_COOKIE['user_id']."'");
    $user_row = mysqli_fetch_object($user);
?>

<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SocialBlock</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="css/style.min.css" rel="stylesheet" media="screen">
    <? echo "<link href='css/backgrounds/background".$user_row->design.".css' rel='stylesheet' type='text/css'/>"; ?>
    <link href="css/template.css" rel="stylesheet" type="text/css"/>
    <link href="css/template-profile.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="chrome-extension://bfbmjmiodbnnpllbbbfblcplfjjepjdn/js/injected.js"></script></head>


    <body style="">
        <div class="container">
            <? require("inc/navbar.inc.php"); # Left Nav/Mainbar ?>

            <div id="wrap">
                <?
                	if(!$_GET['menu']){require("inc/main/main.inc.php");}
                	if($_GET['menu'] == "my-profile"){require("inc/main/my-profile.inc.php");}
                ?>

                <footer id="footer">
                    <p>© <? echo date("Y"); ?> <a href="http://blockentertainment.de" target="_blank">BlockEntertainment</a>. Alle Rechte vorbehalten.</p>
                </footer>
            </div>
        </div>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/app.js"></script>

</body>
</html>