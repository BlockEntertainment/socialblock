<section id="authenty_preview">
	<section id="password_recovery" class="authenty password-recovery">
		<div class="section-content">
			<div class="wrap">
				<div class="container">
					<div class="form-wrap">
						<div class="row">
							<div class="col-xs-12 col-sm-3 brand">
								<h2>SocialBlock</h2>
								<p>Authentication is good<br>Hacking is bad</p>
							</div>
							<div class="col-sm-1 hidden-xs">
								<div class="horizontal-divider"></div>
							</div>
							<div class="col-xs-12 col-sm-8 main">
								<h2>Forgot your password?</h2>
								<p>Not to worry. Just enter your email address below and we'll send you an instruction email for recovery.</p>
								<form method="post">
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Email" required="required">
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-4 col-sm-offset-8">
											<button type="submit" class="btn btn-block reset">Reset Password</button>
										</div>
									</div>
								</form>	
							</div>
						</div>			
					</div>
				</div>
			</div>
		</div>
	</section>
</section>