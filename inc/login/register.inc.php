<section id="authenty_preview">
	<section id="signup_window" class="authenty signup-window">
		<div class="section-content">
			<div class="wrap">
				<div class="container">
					<div class="row form-wrap">
						<div class="title">
							<h3>Create an account</h3>
						</div>
						<?
							if(isset($_POST['sub_register'])){
								$ip = $_SERVER['REMOTE_ADDR'];
								$firstname = $_POST['firstname'];
								$lastname = $_POST['lastname'];
								$email = $_POST['email'];
								$password = hash("sha512", $_POST['password']);
									$sql = mysqli_query($db, "SELECT * FROM users WHERE email = '".mysqli_real_escape_string($db, $email)."'");
									$row = mysqli_fetch_object($sql);
								if($email != $row->email){
									mysqli_query($db, "INSERT INTO users (ip, 
																	 firstname, 
																	 lastname, 
																	 email, 
																	 password) 
														VALUES ('".mysqli_real_escape_string($db, $ip)."',
																'".mysqli_real_escape_string($db, $firstname)."',
																'".mysqli_real_escape_string($db, $lastname)."',
																'".mysqli_real_escape_string($db, $email)."',
																'".mysqli_real_escape_string($db, $password)."')");
									if(mysql_error()){exit(mysql_error());}
									header("Location: login.php?login=register&success=1");
								}else{echo "<div class='alert alert-danger'>The specified E-Mail address is already in use.</div>";}
							}

							if($_GET['success'] == "1"){echo "<div class='alert alert-success'>You have successfully registered. Please <a href='?login=login'>log in</a> now.</div>";}
						?>
						<div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1">
							<div class="sns-signin">
								<a class="btn btn-primary" href="#">
									<i class="fa fa-facebook"></i>Sign in via Facebook</a>
							</div>
						</div>
						<div class="col-xs-12 col-md-2 col-sm-1">
							<div class="horizontal-divider"></div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="normal-signup">
								<form method="post">
									<div class="form-group">
										<input type="text" name="firstname" class="form-control" placeholder="First name" required>
										<input type="text" name="lastname" class="form-control" placeholder="Last name" required>
										<input type="email" name="email" class="form-control" placeholder="Email" required>
										<input type="password" name="password" class="form-control" placeholder="Password" required>
									</div>
									<div class="row">
										<div class="col-md-5 col-md-offset-7">
											<button type="submit" name="sub_register" class="btn btn-block signup">Sign Up</button>
										</div>
									</div>
								</form>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>