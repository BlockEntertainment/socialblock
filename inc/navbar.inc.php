<?
    require("inc/navbar-func/friends.inc.php");
    require("inc/navbar-func/search.inc.php");
    require("inc/navbar-func/support.inc.php");
	
    $user_conn = mysqli_query($db, "SELECT * FROM users WHERE id = '".$_COOKIE['user_id']."'");
	$user_conn_row = mysqli_fetch_object($user_conn);
?>

<nav id="mainbar" class="hidden-xs">
    <ul class="list-unstyled" id="mainmenu">
    	<span style="font-size: 13px;">
    		<? echo $user_conn_row->firstname." ".substr($user_conn_row->lastname, 0,1)."."; ?>
    	</span><hr/>
        <li><a href="index.php" class="smooth"><i class="fa fa-home"></i>Startseite</a></li>
        <li><a href="index.php?menu=my-profile" class="smooth"><i class="fa fa-user"></i>Mein Profil</a></li>
        <li><a class="smooth" data-toggle="modal" data-target="#friends"><i class="fa fa-heart"></i>Freunde</a></li>
        <li><a href="?menu=groups" class="smooth"><i class="fa fa-group"></i>Gruppen</a></li>
        <li><a class="smooth" data-toggle="modal" data-target="#search"><i class="fa fa-search"></i>Suche</a></li>
        <li><a class="smooth" data-toggle="modal" data-target="#support"><i class="fa fa-envelope-o"></i>Support</a></li>
    </ul>
</nav>