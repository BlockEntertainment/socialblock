<!-- Modal -->
<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg">
		<div class="modal-content">
	  		<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Suche</h4>
	  		</div>
	  		<div class="modal-body">
				<form action="search.php" method="get">
					<div class="row">
						<div class="col-md-10">
							<input type="text" name="q" class="form-control" placeholder="Suche" autofocus>
						</div>
						<div class="col-md-2">
							<button type="submit" class="modal-btn btn-block modal-btn-primary">Suchen</button>
						</div>
					</div>
				</form>
	  		</div>
	  		<div class="modal-footer">
				<button type="button" class="modal-btn modal-btn-default" data-dismiss="modal">Schließen</button>
	  		</div>
		</div>
  	</div>
</div>