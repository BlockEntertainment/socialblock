<!-- Modal -->
<div class="modal fade" id="friends" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Meine Freunde</h4>
			</div>
			<div class="modal-body modal-style">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
				  	<li class="active"><a href="#all-friends" class="btn-link" data-toggle="tab">Alle Freunde</a></li>
				  	<li><a href="#online-friends" class="btn-link" data-toggle="tab">Online Freunde</a></li>
				  	<li><a href="#offline-friends" class="btn-link" data-toggle="tab">Offline Freunde</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  	<div class="tab-pane fade in active" id="all-friends"><br>
				  		<? require("inc/navbar-func/friend_status/all_friends.inc.php"); ?>
				  	</div>
				  	<div class="tab-pane fade in" id="online-friends"><br>
						<? require("inc/navbar-func/friend_status/online.inc.php"); ?>
				  	</div>
				  	<div class="tab-pane fade in" id="offline-friends"><br>
						<? require("inc/navbar-func/friend_status/offline.inc.php"); ?>
				  	</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="modal-btn modal-btn-default" data-dismiss="modal">Schließen</button>
			</div>
		</div>
	</div>
</div>