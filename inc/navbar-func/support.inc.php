<!-- Modal -->
<div class="modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg">
		<div class="modal-content">
	  		<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Support</h4>
	  		</div>
	  		<div class="modal-body">
				<form class="form-horizontal" role="form">
				  	<div class="form-group">
				    	<label class="col-sm-2 control-label modal-control-label-color">Betreff</label>
				    	<div class="col-sm-10">
				      		<input type="text" class="form-control" placeholder="Betreff">
				    	</div>
				  	</div>
				  	<div class="form-group">
				    	<label class="col-sm-2 control-label modal-control-label-color">Beschreibung</label>
				    	<div class="col-sm-10">
				     		<textarea required></textarea>
				    	</div>
				  	</div>
				  	<div class="form-group">
				    	<div class="col-sm-offset-2 col-sm-10">
				      		<button type="submit" class="btn btn-default">Sign in</button>
				    	</div>
				  	</div>
				</form>
	  		</div>
	  		<div class="modal-footer">
				<button type="button" class="modal-btn modal-btn-default" data-dismiss="modal">Schließen</button>
	  		</div>
		</div>
  	</div>
  	<!--
  	<script src="js/tinymce/tinymce.min.js"></script>
	<script>
	        tinymce.init({selector:'textarea'});
	</script>
	-->
</div>