<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
		  	<div class="panel-heading">Info</div>
		  	<div class="panel-body">
		    	Geht zur Schule &quot;JBH&quot;
		    	<hr/>
		    	Wohnt in Hamburg
		    	<hr/>
		    	Aus Hamburg
		  	</div>
		</div>

		<div class="panel panel-default">
		  	<div class="panel-heading">Fotos</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110135-2.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110135-2.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110135-2.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    	</div>
		    	<br>
		    	<div class="row">
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110134-1.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110135-2.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110135-2.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    	</div>
		    	<br>
		    	<div class="row">
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110135-2.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110134-1.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    		<div class="col-md-4">
		    			<center><img src="image/profile-pics/201405110135-2.jpg" class="img-responsive chronik-pics-fieldsize"></center>
		    		</div>
		    	</div>
		  	</div>
		</div>

		<div class="panel panel-default">
		  	<div class="panel-heading">Freunde</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-4">
		    			FREUND 1
		    		</div>
		    		<div class="col-md-4">
		    			FREUND 2
		    		</div>
		    		<div class="col-md-4">
		    			FREUND 3
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-4">
		    			FREUND 4
		    		</div>
		    		<div class="col-md-4">
		    			FREUND 5
		    		</div>
		    		<div class="col-md-4">
		    			FREUND 6
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-4">
		    			FREUND 7
		    		</div>
		    		<div class="col-md-4">
		    			FREUND 8
		    		</div>
		    		<div class="col-md-4">
		    			FREUND 9
		    		</div>
		    	</div>
		  	</div>
		</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
		  	<div class="panel-heading">Status</div>
		  	<div class="panel-body">
		    	Panel content
		  	</div>
		</div>

		<div class="panel panel-default">
		  	<div class="panel-heading"><b>Justin Dittmer</b> hat eine Statusmeldung hier hinterlassen.</div>
		  	<div class="panel-body">
		    	Panel content
		  	</div>
		</div>
	</div>
</div>