<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
		  	<div class="panel-heading">Arbeit und Ausbildung</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-2">
		    			ICON
		    		</div>
		    		<div class="col-md-10">
		    			<strong>Riboli</strong><br>
		    			<small>Praktika als Informatiker &bull; Hamburg &bull; 14. Oktober 2013 bis heute</small>
		    		</div>
		    	</div>
		    	<hr/>
		    	<div class="row">
		    		<div class="col-md-2">
		    			ICON
		    		</div>
		    		<div class="col-md-10">
		    			<strong>BlockEntertainment</strong><br>
		    			<small>CEO &bull; Finkenwerder, Hamburg, Germany &bull; September 2011 bis heute</small>
		    		</div>
		    	</div>
		  	</div>
		</div>

		<?
			if(!$_GET['id'] || $_GET['id'] == $_COOKIE['user_id']){
				$user_knowledge = mysqli_query($db, "SELECT * FROM users_profile_knowledge WHERE id = '".$_COOKIE['user_id']."'");
				$user_knowledge_row = mysqli_fetch_object($user_knowledge);
			}else{
				$user_knowledge = mysqli_query($db, "SELECT * FROM users_profile_knowledge WHERE id = '".$_GET['id']."'");
				$user_knowledge_row = mysqli_fetch_object($user_knowledge);
			}
		?>

		<div class="panel panel-default">
		  	<div class="panel-heading">Berufliche Kenntnisse</div>
		  	<div class="panel-body">
		    	<?
		    		if(strlen($user_knowledge_row->knowledge) > 0){
		    			echo str_replace(",", "<br>", "$user_knowledge_row->knowledge");
		    		}else{
		    			echo "-";
		    		}
		    	?>
		  	</div>
		</div>

		<div class="panel panel-default">
		  	<div class="panel-heading">Beziehungsstatus</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-6">
		    			FOTO
		    		</div>
		    		<div class="col-md-6">
		    			Füge deine Beziehung hinzu
		    		</div>
		    	</div>
		  	</div>
		</div>

		<div class="panel panel-default">
		  	<div class="panel-heading">Familie</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-6">
		    			FOTO
		    		</div>
		    		<div class="col-md-6">
		    			Jörn Geniusatwork<br><small>Vater (Bestätigung ausstehend)</small>
		    		</div>
		    	</div>
		    	<hr/>
		    	<div class="row">
		    		<div class="col-md-6">
		    			FOTO
		    		</div>
		    		<div class="col-md-6">
		    			Fabian Dittmer<br><small>Cousin</small>
		    		</div>
		    	</div>
		  	</div>
		</div>

		<div class="panel panel-default">
		  	<div class="panel-heading">Seiten</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-6">
		    			FOTO
		    		</div>
		    		<div class="col-md-6">
		    			Chaosdidi
		    		</div>
		    	</div>
		  	</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-default">
		  	<div class="panel-heading">Ehemalige Wohnorte</div>
		  	<div class="panel-body">
		    	Hamburg<br><small>Aktueller Wohnort</small>
		    	<hr/>
		    	Hamburg<br><small>Heimatstadt</small>
		  	</div>
		</div>

		<?
			if(!$_GET['id'] || $_GET['id'] == $_COOKIE['user_id']){
				$user_general = mysqli_query($db, "SELECT * FROM users_profile_general WHERE id = '".$_COOKIE['user_id']."'");
				$user_general_row = mysqli_fetch_object($user_general);
			}else{
				$user_general = mysqli_query($db, "SELECT * FROM users_profile_general WHERE id = '".$_GET['id']."'");
				$user_general_row = mysqli_fetch_object($user_general);
			}
		?>

		<div class="panel panel-default">
		  	<div class="panel-heading">Allgemeines</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-4">Geburtsdatum</div>
		    		<div class="col-md-8"><? echo $user_general_row->date_of_birth; ?></div>

		    		<div class="col-md-4">Geburtsjahr</div>
		    		<div class="col-md-8"><? echo $user_general_row->year_of_birth; ?></div>

		    		<div class="col-md-4">Geschlecht</div>
		    		<div class="col-md-8"><? echo $user_general_row->sex; ?></div>

		    		<div class="col-md-4">Interessiert an</div>
		    		<div class="col-md-8"><? echo $user_general_row->interested_in; ?></div>

		    		<div class="col-md-4">Beziehungsstatus</div>
		    		<div class="col-md-8"><? echo $user_general_row->relationship_status; ?></div>

		    		<div class="col-md-4">Sprachen</div>
		    		<div class="col-md-8"><? echo $user_general_row->languages; ?></div>

		    		<div class="col-md-4">Religiöse Ansichten</div>
		    		<div class="col-md-8"><? echo $user_general_row->religious_views; ?></div>

		    		<div class="col-md-4">Politische Einstellung</div>
		    		<div class="col-md-8"><? echo $user_general_row->political_setting; ?></div>
		    	</div>
		  	</div>
		</div>

		<?
			if(!$_GET['id'] || $_GET['id'] == $_COOKIE['user_id']){
				$user_contact = mysqli_query($db, "SELECT * FROM users_profile_contact WHERE id = '".$_COOKIE['user_id']."'");
				$user_contact_row = mysqli_fetch_object($user_contact);
			}else{
				$user_contact = mysqli_query($db, "SELECT * FROM users_profile_contact WHERE id = '".$_GET['id']."'");
				$user_contact_row = mysqli_fetch_object($user_contact);
			}
		?>

		<div class="panel panel-default">
		  	<div class="panel-heading">Kontakt</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-4">Handy</div>
		    		<div class="col-md-8"><? echo $user_contact_row->handy; ?></div>

		    		<div class="col-md-4">Adresse</div>
		    		<div class="col-md-8"><? echo $user_contact_row->address; ?></div>

		    		<div class="col-md-4">Nutzernamen</div>
		    		<div class="col-md-8">
		    			<? if($user_contact_row->twitter == "-"){echo "&nbsp;";}else{echo "<a href='#'>".$user_contact_row->twitter."</a> (Twitter)<br>";} ?>
		    			<? if($user_contact_row->skype == "-"){echo "&nbsp;";}else{echo "<a href='#'>".$user_contact_row->skype."</a> (Skype)<br>";} ?>
		    			<? if($user_contact_row->instagram == "-"){echo "&nbsp;";}else{echo "<a href='#'>".$user_contact_row->instagram."</a> (Instagram)";} ?>
		    		</div>

		    		<div class="col-md-4">Webseite</div>
		    		<div class="col-md-8"><? echo $user_contact_row->website; ?></div>

		    		<div class="col-md-4">E-Mail-Adresse</div>
		    		<div class="col-md-8"><? echo $user_contact_row->email_address; ?></div>
		    	</div>
		  	</div>
		</div>
	</div>
</div>