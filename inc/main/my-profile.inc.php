<section>
	<?
		#############################
		### PROFILE TITLE [start] ###
		#############################
		if(!$_GET['id'] || $_GET['id'] == $_COOKIE['user_id']){
			$user_conn = mysqli_query($db, "SELECT * FROM users WHERE id = '".mysqli_real_escape_string($db, $_COOKIE['user_id'])."'");
			$user_row = mysqli_fetch_object($user_conn);
			echo "
				<h1 class='page-header'>
					<div class='row profile-row-pic'>
						<div class='col-md-2'>
							<center>
								<img src='image/profile-pics/".$user_row->cur_profilepic."' class='img-responsive profile-pic'>
							</center>
						</div>
						<div class='col-md-10 profile-page-header-title'>
							Mein Profil<br>
							<a href='#' class='btn btn-default'>Profil bearbeiten</a>
						</div>
					</div>
				</h1>
			";
		}else{
			$user_conn = mysqli_query($db, "SELECT * FROM users WHERE id = '".mysqli_real_escape_string($db, $_GET['id'])."'");
			$user_row = mysqli_fetch_object($user_conn);
			$user_friends = mysqli_query($db, "SELECT * FROM friends WHERE from_id = '".$_COOKIE['user_id']."' AND to_id = '".mysqli_real_escape_string($db, $_GET['id'])."'");
			$user_f_row = mysqli_fetch_object($user_friends);

			require("inc/main/profile/friend-func.inc.php");
			?>
				<h1 class="page-header">
					<div class="row profile-row-pic">
						<div class="col-md-2">
							<center>
								<? echo "<img src='image/profile-pics/".$user_row->cur_profilepic."' class='img-responsive profile-pic'>"; ?>
							</center>
						</div>
						<div class="col-md-10 profile-page-header-title">
							<? echo $user_row->firstname." ".$user_row->lastname; ?>'s Profil<br>
							<form method="post">
								<?
									if($user_f_row->from_id != $_COOKIE['user_id'] && $user_f_row->to_id != $_GET['id']){
										echo "<button type='submit' name='add_friend' class='btn btn-default'>Freund/in hinzufügen</button>";
									}
									if($user_f_row->status == "0"){
										echo "<button type='submit' name='add_cancel' class='btn btn-default'>Freundschaftsanfrage abbrechen</button>";
									}
									if($user_f_row->status == "1"){
										echo "<button type='submit' name='add_friendadd' class='btn btn-default'>Freundschaftsanfrage annehmen</button>";
									}
									if($user_f_row->status == "2"){
										echo "<button type='submit' name='del_friend' class='btn btn-default'>Freund/in entfernen</button>";
									}
								?>
							</form>
						</div>
					</div>
				</h1>
		<? }

		###########################
		### PROFILE TITLE [end] ###
		###########################
	?>
	<?
		if(!$_GET['id'] || $_GET['id'] == $_COOKIE['user_id']){
			$user_conn = mysqli_query($db, "SELECT * FROM users WHERE id = '".mysqli_real_escape_string($db, $_COOKIE['user_id'])."'");
			$user_row = mysqli_fetch_object($user_conn);
				$friend_count = $user_row->friends;
		}else{
			$user_conn = mysqli_query($db, "SELECT * FROM users WHERE id = '".mysqli_real_escape_string($db, $_GET['id'])."'");
			$user_row = mysqli_fetch_object($user_conn);
				$friend_count = $user_row->friends;
		}
	?>

	<!-- Nav tabs -->
	<ul class="nav nav-tabs">
	  	<li class="active"><a href="#chronik" data-toggle="tab">Chronik</a></li>
	  	<li><a href="#info" data-toggle="tab">Info</a></li>
	  	<li><a href="#fotos" data-toggle="tab">Fotos (17)</a></li>
	  	<li><a href="#freunde" data-toggle="tab">Freunde (<? echo $friend_count; ?>)</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
	  	<div class="tab-pane fade in active" id="chronik">
	  		<br>
	  		<? require("inc/main/profile/chronik.inc.php"); ?>
	  	</div>
	  	<div class="tab-pane fade in" id="info">
	  		<br>
	  		<? require("inc/main/profile/info.inc.php"); ?>
	  	</div>
	  	<div class="tab-pane fade in" id="fotos">
	  		
	  	</div>
	  	<div class="tab-pane fade in" id="freunde">
	  		
	  	</div>
	</div>
</section>