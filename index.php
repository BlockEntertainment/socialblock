<html>
    <head>
        <style type="text/css">
            .gm-style .gm-style-mtc label,.gm-style .gm-style-mtc div {
                font-weight: 400;
            }

            .gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div {
                font-size:10px;
            }

            @media print {
                .gm-style .gmnoprint, .gmnoprint {
                    display: none;
                }
            }

            @media screen {
                .gm-style .gmnoscreen, .gmnoscreen {
                    display: none;
                }
            }

            .gm-style {
                font-family: Roboto, Arial, sans-serif;
                font-size: 11px;
                font-weight: 400;
                text-decoration: none
            }

            body {
            	background: url("image/background.jpg") fixed center center no-repeat;
            	background-size: cover;
            }
        </style>

        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SocialBlock</title>

        <link rel="shortcut icon" href="icons/favicon.ico">
        <link rel="apple-touch-icon" href="icons/icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="icons/icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="icons/icon-114x114.png">

        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet" type="text/css">

        <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="plugins/creative-brands/creative-brands.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="css/custom.css" rel="stylesheet" type="text/css"> 
        <link href="css/animate.css" rel="stylesheet" type="text/css">
        <link href="plugins/time-circles/TimeCircles.css" rel="stylesheet" type="text/css">
        <link href="plugins/vegas/css/jquery.vegas.css" rel="stylesheet" type="text/css">
    
        <script type="text/javascript" src="chrome-extension://bfbmjmiodbnnpllbbbfblcplfjjepjdn/js/injected.js"></script>
    </head>
    <body style="">
        
        <div class="vegas-overlay" style="margin: 0px; padding: 0px; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; background-image: url(http://www.billboard.pixelized.cz/plugins/vegas/overlays/05.png);"></div>

    <h1>SocialBlock</h1>

    <section id="modal">     

        <div class="modal fade" id="subscribe" tabindex="-1" role="dialog" aria-labelledby="subscribe" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
              
                            <div class="col-md-12">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3>Abonniere uns</h3>
                            </div>
                
                            <div class="col-md-2 col-sm-3 col-xs-4">
                                <img src="image/mail.png" class="img-responsive img-circle" alt="">                    
                            </div>
                
                            <div class="col-md-4 col-sm-3 col-xs-8">
                                <p>Du möchtest als erstes Benachrichtigt werden wenn unser Social Network vollendet ist? Dann Abonniere SocialBlock über ein einfaches E-Mail Formular.</p>            
                            </div>
                
                            <div class="col-md-6 center-block"><!-- Subscribe form -->
                                <div class="subscribe-wrap">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">
                                            <label class="sr-only">Email Adresse</label>
                                            <input type="email" class="form-control" placeholder="Email Adresse für das Abo eintragen">
                                        </div>
                                        <button type="submit" class="btn btn-submit">Abonnieren</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
        <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="contact" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 class="text-center">Kontakt</h3>
                            </div>
                            <div class="col-md-4 col-sm-4"></div>
                            <div class="col-md-4 col-sm-4"><!-- Address -->
                                <address class="text-center">
                                    <h4>BlockEntertainment</h4><br>
                                    Justin Dittmer<br>
                                    21129 Hamburg<br>
                                    +49 (0) 162 6871189
                                </address>
                      
                                <address class="text-center">
                                    <a href="mailto:admin@blockentertainment.de">admin@blockentertainment.de</a>
                                </address>
                            </div>
                            <div class="col-md-4 col-sm-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
        <div class="modal fade" id="work" tabindex="-1" role="dialog" aria-labelledby="work" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
              
                            <div class="col-md-12">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3>Über unsere Arbeit</h3>
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <img src="image/bolt.png" class="img-responsive img-circle" alt="">
                                <h4>Software</h4>
                                <p class="center-block">Sublime Text 3, WinSCP</p>
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <img src="image/rocket.png" class="img-responsive img-circle" alt="">
                                <h4>Framework</h4>
                                <p class="center-block">Bootstrap</p>
                            </div>

                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <img src="image/dev.png" class="img-responsive img-circle" alt="">
                                <h4>Sprachen</h4>
                                <p class="center-block">HTML, PHP, CSS, JavaScript</p>
                            </div>
                
                        </div>
                    </div>
                </div>
            </div>
      </div>
    
    </section>
    
    <div class="container"> 
        <div class="row" id="text"><!-- start of row text -->
            <div class="col-md-12">
                <h2>Demnächst</h2>
                <p>
                    Wir arbeiten gerade an einem eigenen <b>Social Network</b>.<br>
                    Facebook? Naja, nicht unbedingt. Das Design wird Modern und einfach sein.<br>
                    Einfach abwarten ...
                </p>

           </div>
        </div><!-- end of row text -->
        
        <div class="row countdown"><!-- start of row countdown -->
        
            <div class="col-md-12"><!-- Line Separator -->
                <div class="line"><h3>Wie lange noch?</h3></div>
                <div id="DateCountdown" data-date="2014-05-31 20:00:00" data-tc-id="1cfc257a-114f-8262-99f5-6d2f92bcdbad">
                    <div class="time_circles">
                        <canvas height="125" width="500"></canvas>
                        <div class="textDiv_Days" style="top: 44px; left: 0px; width: 125px;">
                            <h4>Tage</h4>
                            <span></span>
                        </div>
                        <div class="textDiv_Hours" style="top: 44px; left: 125px; width: 125px;">
                            <h4>Stunden</h4>
                            <span></span>
                        </div>
                        <div class="textDiv_Minutes" style="top: 44px; left: 250px; width: 125px;">
                            <h4>Minuten</h4>
                            <span></span>
                        </div>
                        <div class="textDiv_Seconds" style="top: 44px; left: 375px; width: 125px;">
                            <h4>Sekunden</h4>
                            <span></span>
                        </div>
                    </div>
                </div><!-- Circle Countdown here you can set up time to countdown just simply change atribute data-date="yyyy-mm-dd time" -->
            </div> 
            
        </div><!-- end of row countdown -->
        
    </div><!-- end of container -->
    
    <footer class="navbar navbar-default navbar-fixed-bottom" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs" href="#">SocialBlock</a>
            </div>
    
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="brands brands-inline navbar-left">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li> 
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#" data-toggle="modal" data-target="#subscribe">Abonnieren</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#work">Unsere Arbeit</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#contact">Kontakt</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </footer>
      
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/custom.js"></script>
    <script src="plugins/vegas/js/jquery.vegas.js"></script>
    <script src="plugins/creative-brands/creative-brands.js"></script>
    <script src="plugins/time-circles/TimeCircles.js"></script>

    </body>
</html>